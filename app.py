from flask import Flask, render_template
import requests
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField
from wtforms.validators import DataRequired, Length

URL = "https://newsapi.org/v2/everything"
API_KEY = "paste api key"
results_number = [(10, '10'), (20, '20'), (30, '30'), (40, '40'), (50, '50')]

# App Config
app = Flask(__name__)
app.secret_key = "2!j@56czUkTQ53"

headers = {
	"X-Api-Key": API_KEY
}
parameters = {
	"language": "en",
	"sortBy": "publishedAt"
}


# Forms
class SearchForm(FlaskForm):
	search = StringField('Search Query', validators=[DataRequired(), Length(min=3, max=40)], render_kw={'class': 'form-control', 'placeholder': 'type here'})
	results = SelectField('Results', choices=results_number, render_kw={'class': 'form-select'})
	submit = SubmitField('Search', render_kw={'class': 'btn btn-light btn'})


# Routes
@app.route('/', methods=['POST', 'GET'])
def index():
	form = SearchForm()
	if form.validate_on_submit():
		parameters['q'] = form.search.data
		parameters['pageSize'] = form.results.data
		response = requests.get(url=URL, headers=headers, params=parameters)
		return render_template('index.html', form=form, data=response.json())
	else:
		return render_template('index.html', form=form)


# App Run
if __name__ == '__main__':
	app.run(debug=True)
