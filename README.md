# WebApi News Site
A News website that gets data from **https://newsapi.org/** using API.  
User enters a search query and number of results.

Tech: Python, Flask, API, Bootstrap